__author__ = 'Administrator'
def autosuggest(prefix,items):
    outputlist=[]
    for item in items:
        if(item.startswith(prefix)==True):
            outputlist.append(item)
    return outputlist


def returnfoodlist(foods=['pizza','cakes','chocolates','ice-creams','donuts','shakes','rice','rolls','pastries']):
    # return foods
    # foods=fetch('select food from foods')
    # foods=readalllinesfromfile('foodname.txt')
    return foods

def returnfooddata(filename='items.csv'):
    import csv
    foods=[]
    with open(filename) as fooditems:
        reader = csv.DictReader(fooditems)
        for row in reader:
            foods.append(row)
    return foods
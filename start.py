__author__ = 'anurag'
from flask import Flask,request
from flask import render_template
from flask import jsonify
from utility import autosuggest,returnfoodlist

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello Worldddddd!'

@app.route('/ajaxtest')
def ajaxt():
    return jsonify(listoutput=[1,2,3,4,5])

@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)




@app.route('/getautosuggest/')
def getautosuggestajax():
    prefix=request.args['pre']
    if(prefix==None):
        return jsonify([])
    foods=returnfoodlist()
    return jsonify(results=autosuggest(prefix,foods))


if __name__ == '__main__':
    app.run(debug=True)